# Deliver #
 
### O Projeto é um novo sistema de entrega de mercadorias que possui um diferencial importante, prover o menor caminho de entrega possível diminuindo assim consideravelmente os custos. Vale salienta que todos os mapas devem ser persistidos para composição de uma base histórica para futura mineração de dados, visando prover mais informações para prospecção de novos negócios. ### 
 
## Arquitetura ##
 
Para implementação do diferencial do sistema de entregas que visa obter o menor custo, foi estudado a implementação do problema clássico do Cacheiro Viajante, cujo objetivo de determinar o menor circuito dado um determinado mapa, vale salientar que os mapas devem ser persistidos na base de dados. Visando atender esta necessidade foi definido a utilização de um banco de grafos Neo4j. 
 
Após a definição da base dados foi iniciado a criação da estrutura da aplicação. A aplicação foi divida em quatro módulos (beans, repository, services e web) objetivando aumenta a coesão e diminuir o acoplamento entre as camadas (módulos) aumentando consideravelmente a manutenibilidade que é apoiada com a utilização do FrameWork Spring Data. 
 
As funcionalidades do sistema foram expostas como APIs REST através do framework
 
## AVISO ##
 
Por padrão, o aplicativo tentará usar uma instância Neo4j rodando na mesma máquina no endereço http://localhost:7474, por padrão durante a execução do teste  e feito um expurgo total da base  o que vai destruir todos os dados do banco. Então, se você não quer que isso aconteça, favor fazer backup do banco de dados existente em primeiro lugar.
 
## Pré -Requisitos ##

    * Java 7 ou superior
    * Neo4J

 
## Configuração ##
 
Antes de iniciarmos a aplicação é importante realizar a configuração de acesso a base de dados no nosso caso Neo4j. Para isso devemos altera as configurações do arquivo ogm.properties que esta localizado no módulo deliver-repository/src/main/resources.
 
## Instalação ##
 
Para instalação e necessário realizar o download do código-fonte git clone [https://< Usuario >@bitbucket.org/rodrigorcarv/deliver.git ](Link URL).
 
Após realizar o download do código-fonte deve ser executados os comandos abaixo:

    * mvn eclipse:eclipse  
    * mvn clean install

A seguir basta copiar o WAR disponível no módulo .../deliver-web/target/deliver-web.war e copiá-lo para o diretório TOMCAT_HOME/webapps.
 
### Explorando API REST ###
 
    * SaveRoutes


```
#!java


PUT /deliver-web/mapa/saveRoutes/{nome do mapa} HTTP/1.1
 
Host: localhost:9080
Accept: text/plain
Content-Type: text/plain
Cache-Control: no-cache
 
A B 10
B D 15 
A C 20 
C D 30 
B E 50 
D E 30

```


    * Save 


```
#!java

PUT /deliver-web/mapa/salvar/{nome do mapa} HTTP/1.1
Host: localhost:9080
Accept: application/json
Content-Type: application/json
Cache-Control: no-cache
 
[
  {
    "cidadeOrigem": {
      "nome": "A"
    },
    "cidadeDestino": {
      "nome": "C"
    },
    "distancia": 20
  },
  {
    "cidadeOrigem": {
      "nome": "B"
    },
    "cidadeDestino": {
      "nome": "E"
    },
    "distancia": 50
  },
  {
    "cidadeOrigem": {
      "nome": "B"
    },
    "cidadeDestino": {
      "nome": "D"
    },
    "distancia": 15
  },
  {
    "cidadeOrigem": {
      "nome": "D"
    },
    "cidadeDestino": {
      "nome": "E"
    },
    "distancia": 30
  },
  {
    "cidadeOrigem": {
      "nome": "A"
    },
    "cidadeDestino": {
      "nome": "B"
    },
    "distancia": 10
  },
  {
    "cidadeOrigem": {
      "nome": "C"
    },
    "cidadeDestino": {
      "nome": "D"
    },
    "distancia": 30
  }
]
 
```

      * shortestRouteDeliver

```
#!java


GET /deliver-web/mapa/shortestRouteDeliver?mapaNome={nome do mapa}&origem={cidade de origem}&destino={cidade de destino}&autonomia={autonomia}&valorCombustivel={valor do combustivel} HTTP/1.1
 
Host: localhost:9080
Cache-Control: no-cache
```